@extends('layouts.app')
@section('title', 'Posts')
@section('content')
@include('parts.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <section class="h-100">

            @foreach ($orders as $order)

            <section class="h-100 gradient-custom">
                <div class="container py-5 h-100">
                  <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-lg-10 col-xl-8">
                      <div class="card" style="border-radius: 10px;">
                        <div class="card-header px-4 py-5">
                          <h5 class="text-muted mb-0">Thanks for your Order, <span style="color: #a8729a;">{{$order->name}}</span>!</h5>
                        </div>
                        <div class="card-body p-4">
                            <div class="d-flex justify-content-between align-items-center mb-4">
                              <p class="lead fw-normal mb-0" style="color: #a8729a;">Receipt</p>
                              <p class="small text-muted mb-0">Order id : {{$order->id}}</p>
                            </div>

                        @foreach ($order->orderProducts as $item)

                            <div class="card shadow-0 border mb-4">
                              <div class="card-body">
                                <div class="row">
                                  <div class="col-md-2">

                                    @if ($item->product->image_path )
                                    <img src="{{ Storage::url( $item->product->image_path) }} "
                                      class="img-fluid" alt="Phone">
                                    @endif

                                  </div>
                                  <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                                    <p class="text-muted mb-0">{{ $item->product->title }}</p>
                                  </div>
                                  <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                                    <p class="text-muted mb-0 small">{{$item->product->preview}}</p>
                                  </div>

                                  <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                                    <p class="text-muted mb-0 small">Qty: {{ $item->count }}</p>
                                  </div>
                                  <div class="col-md-2 text-center d-flex justify-content-center align-items-center">
                                    <p class="text-muted mb-0 small">{{ $item->price }}$</p>
                                  </div>
                                </div>
                                <hr class="mb-4" style="background-color: #e0e0e0; opacity: 1;">
                                <div class="row d-flex align-items-center">
                                  <div class="col-md-2">
                                    <p class="text-muted mb-0 small">Track Order</p>
                                  </div>
                                  <div class="col-md-10">
                                    <div class="progress" style="height: 6px; border-radius: 16px;">
                                      <div class="progress-bar" role="progressbar"
                                        style="width: 65%; border-radius: 16px; background-color: #a8729a;" aria-valuenow="65"
                                        aria-valuemin="0" aria-valuemax="100"></div>
                                    </div>
                                    <div class="d-flex justify-content-around mb-1">
                                      <p class="text-muted mt-1 mb-0 small ms-xl-5">{{ $order->status }}</p>
                                      <p class="text-muted mt-1 mb-0 small ms-xl-5">Delivered</p>
                                    </div>
                                  </div>
                                </div>
                              </div>
                            </div>
                        @endforeach


                          <div class="card shadow-0 border mb-4">


                          <div class="d-flex justify-content-between pt-2">
                            <p class="fw-bold mb-0">Order Details</p>
                          </div>



                          <div class="d-flex justify-content-between">
                            <p class="text-muted mb-0">Invoice Date : {{ $order->created_at }}</p>

                          </div>


                        <div class="card-footer border-0 px-4 py-5"
                          style="background-color: #a8729a; border-bottom-left-radius: 10px; border-bottom-right-radius: 10px;">
                          <h5 class="d-flex align-items-center justify-content-end text-white text-uppercase mb-0">Total
                            paid: <span class="h2 mb-0 ms-2">{{ $order->sum }}$</span></h5>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>

            @endforeach

          </section>


    </div>
</main>
@endsection
