<!DOCTYPE html>
<html lang="en">

<head>
    @include('part.head')

    @livewireStyles

</head>

<body>

    @include('part.header')

    @yield('content')

    @include('part.footer')

    @include('part.bodyscripts')

    @livewireStyles
</body>
</html>
