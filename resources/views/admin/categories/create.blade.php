@extends('admin.layouts.app')
@section('title', 'Create category')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <form method="POST" action=" {{route('admin.category.store') }}">

            @csrf
            <div class="row mb-3">
                <label for="name" class="col-sm-2 col-form-label">Category:</label>
                    <div class="col-sm-10">
                        <input type="string" class="form-control" id="category" placeholder="Enter name category" name="name">
                            @error('name')
                                <p class="alert alert-danger">{{$message}}</p>
                            @enderror
                    </div>
                </div>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit</button>
                </div>
        </form>
    </div>
</main>
@endsection
