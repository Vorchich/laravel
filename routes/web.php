<?php

use App\Http\Controllers\CartController;
use App\Http\Controllers\PostController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\ItemController;
use App\Http\Controllers\OrderController;
use Illuminate\Support\Facades\Route;
use Illuminate\Support\Facades\Auth;


Auth::routes();

Route::prefix('/category')->name('category.')->group( function () {
    Route::get('/', [CategoryController::class,'index'])->name('index');
    Route::get('/{category}', [CategoryController::class,'show'])->name('show');
    Route::get('/{category}/posts', [PostController::class,'index'])->name('post.index');
    Route::get('/{category}/{post}', [PostController::class,'show'])->name('post.show');
});
Route::get('/',ItemController::class)->name('home');
Route::prefix('/cart')->name('cart.')->group( function () {

    Route::post('/add',[CartController::class, 'addToCart'])->name('add');
    Route::delete('/remove/{cartproduct}',[CartController::class, 'remove'])->name('remove');
    Route::post('/promo',([CartController::class, 'promo']))->name('promo');
    Route::delete('/remove',[CartController::class, 'promoDelete'])->name('promo.delete');

});
Route::prefix('/order')->name('order.')->group( function () {

    Route::post('/create',[OrderController::class, 'create'])->name('create');
    Route::get('/show',[OrderController::class, 'show'])->name('show');

});

// Route::post('/add-to-cart',[CartController::class, 'addToCart'])->name('addToCart');
// Route::delete('/cartproduct/{cartproduct}',[CartController::class, 'remove'])->name('remove');
Route::get('/cart',[CartController::class, 'cart'])->name('cart');
Route::post('/checkpromo',[CartController::class, 'checkPromo'])->name('checkPromo');


