<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">

        <li class="nav-item">
            <a class="nav-link " href="{{route('admin.category.index')}}">
                <i class="bi bi-grid"></i>
                <span>Categories</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="{{route('admin.promo.index')}}">
                <i class="bi bi-grid"></i>
                <span>Promocodes</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="{{route('admin.order.index')}}">
                <i class="bi bi-grid"></i>
                <span>Orders</span>
            </a>
        </li>
        <li class="nav-item">
            <a class="nav-link " href="{{route('admin.statistic')}}">
                <i class="bi bi-grid"></i>
                <span>Statistic</span>
            </a>
        </li>

    </ul>
</aside><!-- End Sidebar-->
