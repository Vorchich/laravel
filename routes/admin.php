<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\Admin\AdminCategoryController;
use App\Http\Controllers\Admin\AdminPostController;
use App\Http\Controllers\Admin\OrderController;
use App\Http\Controllers\PromoController;
use App\Http\Controllers\Admin\StatisticController;

Route::resources([
    'category' => AdminCategoryController::class,
    'category.post' => AdminPostController::class,
    'promo' => PromoController::class,
    'order' => OrderController::class,
],[
    'parameters' => [
        'category' => 'category:slug',
    ]
]);

Route::get('/statistic',[StatisticController::class,'statistic'])->name('statistic');
Route::post('/statistic/date',[StatisticController::class,'date'])->name('date');
