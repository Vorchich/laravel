
<div class="card">

    <div class="card-body">
      <h5 class="card-title">{{$post->title}}</h5>
      <p class="card-text">{{$post->preview}}</p>
      <p class="card-text">{{$post->description}}</p>
      <p class="card-text">author: {{$post->user ? $post->user->name : 'без автора'}}</p>
      <a href="{{route('admin.category.index')}}" class="btn btn-primary">Go back</a>
    </div>
  </div>
