<?php

namespace App\Services;


use App\Models\Order;
use App\Models\OrderProducts;

class StatisticService
{

    public function orderStatistic($start = null, $end = null)
    {
        return Order::query()
        ->when($start, function ($builder) use($start) {
            $builder->where('created_at', '>', $start);
        })
        ->when($end, function ($builder) use($end) {
            $builder->where('created_at', '<', $end);
        })
        ->pluck('status')->countBy();
    }

    public function orderProductsStatistic($start = null, $end = null, $top = 1)
    {
        return OrderProducts::query()
        ->when($start, function ($builder) use($start) {
            $builder->where('created_at', '>', $start);
        })
        ->when($end, function ($builder) use($end) {
            $builder->where('created_at', '<', $end);
        })
        ->get()
        ->countBy('name')
        ->take($top);
    }


}
