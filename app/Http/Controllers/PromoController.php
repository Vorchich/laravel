<?php

namespace App\Http\Controllers;

use App\Http\Requests\StorePromoRequest;
use App\Models\Promo;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class PromoController extends Controller
{
    public function index()
    {
        $promos = Promo::paginate();

        return view('admin.promo.index', compact('promos'));
    }

    public function create()
    {
        $types = Promo::getTypes();
        return view('admin.promo.create', compact('types'));
    }

    public function store(StorePromoRequest $req)
    {
        $val = $req->validated();

        if($req->check) {
            $val['code'] =  Str::random(20);
        }

        Promo::create($val);

        return redirect()->route('admin.promo.index');
    }

    public function destroy(Promo $promo)
    {
        $promo->delete();

        return redirect()->route('admin.promo.index');
    }
}
