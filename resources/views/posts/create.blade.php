@extends('layouts.app')
@section('title', 'Create category')
@section('content')
<main id="main" class="main">
    <div class="pagetitle">
    <form method="POST" action="{{ route('admin.category.post.store', $category) }}" enctype="multipart/form-data" >
        @csrf
        <div class="form-group">


            <label for="image">Image:</label>
            <input type="file" class="form-control" name="image">
            @error('image')
                <p class="alert alert-danger">{{$message}}</p>
            @enderror


            <label for="title">Title:</label>
            <input type="string" class="form-control" placeholder="Enter name category" name="title">
            @error('title')
                <p class="alert alert-danger">{{$message}}</p>
            @enderror
          </div>
          <div class="form-group">
            <label for="preview">Preview:</label>
            <input type="string" class="form-control" placeholder="Enter name category" name="preview">
            @error('preview')
                <p class="alert alert-danger">{{$message}}</p>
            @enderror
          </div>
          <div class="form-group">
            <label for="description">Description:</label>
            <input type="string" class="form-control" placeholder="Enter name category" name="description">
            @error('description')
                <p class="alert alert-danger">{{$message}}</p>
            @enderror
          </div>


          <button type="submit" class="btn btn-default">Submit</button>
    </form>
    </div>
</main>
@endsection
