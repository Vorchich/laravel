<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CartProductUpdate extends FormRequest
{
    public function rules()
    {
        return [
            'count' => ['required','numeric','min:0']
        ];
    }
}
