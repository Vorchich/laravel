<?php

namespace App\Console\Commands;

use App\Models\CityNovaPoshta;
use Illuminate\Support\Facades\Http;
use Illuminate\Console\Command;

class SaveCities extends Command
{

    protected $signature = 'cities:update';

    protected $description = 'Update city_nova_poshtas table';

    public function handle()
    {
        $page = 1;
        do {
            $data = [
                'modelName' => "Address",
                'calledMethod' => "getCities",
                'methodProperties' => [
                    'Limit' => 450,
                    'Page' => $page
                ]
            ];
            $cities = Http::post('http://api.novaposhta.ua/v2.0/json/', $data);
            $cities = $cities->json();
            if(count($cities['data'])){
                foreach($cities['data'] as $city){
                    CityNovaPoshta::updateOrCreate([
                        'city_ref' => $city['Ref'],
                    ],[
                        'name' => $city['Description'],
                    ]);
                }
            }

            $page++;
        }   while(!(empty($cities['data'])));
    }
}
