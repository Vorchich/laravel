<?php

namespace App\Http\Controllers;

use App\Http\Requests\PromoRequest;
use App\Services\CartService;
use App\Models\CartProduct;
use App\Models\Promo;
use Illuminate\Http\Request;


class CartController extends Controller
{
    public function addToCart(Request $req, CartService $service)
    {
        $service->updateOrCreate($req->product_id, $req->count);

        return redirect()->back();
    }

    public function remove(CartProduct $cartproduct, CartService $service)
    {
        $service->delete($cartproduct->id);
        // $cartproduct->delete();

        return redirect()->back();
    }

    public function cart(CartService $service)
    {
        $cart = $service->cart;
        if(!($cart->cartProducts->count())){
            $cart->update(['promo_id'=> null]);
        }
            $sum = $service->promoSum($cart->sum);
        return view('parts.promo-cart',compact('cart', 'sum'));
    }

    public function promo(PromoRequest $req, CartService $service)
    {
        $promo = Promo::where('code', '=', $req->code)->firstOrFail();

        $service->promoAddOrCreate($promo);

        return redirect()->route('cart');
    }

    public function promoDelete(CartService $service)
    {
        $service->promoDelete();

        return redirect()->route('cart');
    }
}
