<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Casts\Attribute;

class Cart extends Model
{

    protected $fillable = [
        'user_id',
        'identify',
        'promo_id',
    ];

    use HasFactory;

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function promo()
    {
        return $this->belongsTo(Promo::class);
    }


    public function cartProducts()
    {
        return $this->hasMany(CartProduct::class);
    }


    protected function count(): Attribute
    {
        return Attribute::make(
            get: fn ($value) =>  $this->cartProducts->sum('count'),
        );
    }

    protected function sum(): Attribute
    {
        return Attribute::make(
            get: fn ($value) =>  $this->cartProducts->sum('sum'),
        );
    }

    // protected function promoSum(): Attribute
    // {
    //     return Attribute::make(
    //         get: fn ($value) =>  $this->promo->getSum($this->sum),
    //     );
    // }

    // protected function promoSum(): Attribute
    // {
    //     return Attribute::make(
    //         get: function () {
    //             $sum = $this->sum;
    //             if ($this->promo) {
    //                 switch ($this->promo->type) {
    //                     case Promo::TYPE_NOMINAL:
    //                         $sum -= $this->promo->value;
    //                         break;

    //                     case Promo::TYPE_PERCENT:
    //                         $sum *= (100 - $this->promo->value) / 100;
    //                         break;
    //                 }
    //             }

    //             $sum = $sum < 0 ? 0 : $sum;
    //             return $sum;
    //         },
    //     );
    // }
}
