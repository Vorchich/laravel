<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Post;
use App\Models\Category;

class ItemController extends Controller
{
    public function __invoke()
    {
        $categories = Category::get();
        $posts = Post::where('posted',1)->simplePaginate(25);

        return view('posts.index', compact('categories', 'posts'));
    }
}
