@extends('admin.layouts.app')
@section('title', 'Posts')
@section('content')
    @include('admin.components.sidebar')
    <main id="main" class="main">
        <div class="pagetitle">
            <div class="d-flex justify-content-between">
                <a class="btn btn-primary mb-2" href="{{ route('admin.category.post.create', $category) }}"
                    role="button">Create item</a>

                <h1>{{ $category->name }}</h1>
                <a class="btn btn-primary mb-2" href="{{ route('admin.category.index') }}" role="button">Go to categories</a>
            </div>

            @if ($posts->isNotEmpty())
                @foreach ($posts as $post)
                    @include('admin.parts.item', compact('post'))
                @endforeach
                {{ $posts->links() }}
            @else
                <div class="alert alert-warning" role="alert">
                    Category is empty
                </div>
            @endif
        </div>
    </main>
@endsection
