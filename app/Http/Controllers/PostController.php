<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class PostController extends Controller
{
    public function index(Category $category)
    {
        $categories = Category::get();
        $posts = $category->posts()->latest('created_at')->simplePaginate(3);

        return view('posts.index', compact('posts', 'category','categories'));
    }

    public function show(Category $category, Post $post)
    {
        $categories = Category::get();
        return view('posts.show', compact('post', 'categories'));
    }
}
