<?php

namespace App\Providers;

use App\Models\Cart;
use App\Services\CartService;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        View::composer('part.header', function ($view) {
            $view->with('cart', resolve(CartService::class)->cart);
        });
    }
}
