<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\Statistic\DateRequest;
use App\Services\StatisticService;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;


class StatisticController extends Controller
{
    public function statistic(DateRequest $request, StatisticService $stat)
    {
        $top = $request->top ?? 1;
        $order_statistic = $stat->orderStatistic($request->start, $request->end);
        $products_statistic = $stat->orderProductsStatistic($request->start, $request->end, $top);
        //dd($products_statistic);

        return view('admin.statistic.index', compact('order_statistic', 'products_statistic'));
    }

    // public function date(Request $req, StatisticService $stat)
    // {
    //     $start = $req->start;
    //     $end = $req->end;
    //     $stat->setDate($req->start, $req->end);
    //     $order_statistic = $stat->OrderStatistic($start, $end);

    //     return view('admin.statistic.index', compact('order_statistic', 'start', 'end'));
    // }
}
