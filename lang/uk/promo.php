<?php

use App\Models\Promo;

return [
    'types' => [
        Promo::TYPE_NOMINAL => 'Віднімати вказане значення від суми замовлення',
        Promo::TYPE_PERCENT => 'Знизка на вказаний відоток до суми замовлення',
    ],
];
