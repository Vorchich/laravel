<div class="content">
    <div class="row">
        <div class="card col-md-4" style="width: 13rem">
            <div class="card-body">
            <h5 class="card-title">{{ $category->name }}</h5>
            <p class="card-text">posts count: {{ $category->posts_count }}</p>
            <a href="{{route('category.post.index', ['category' => $category->id])}}" class="btn btn-primary">Go to category</a>
            </div>
        </div>
    </div>

</div>
