@extends('layouts.app')
@section('title', 'Posts')
@section('content')
<section class="h-100 h-custom" style="background-color: #eee;">
    <div class="container py-5 h-100">
      <div class="row d-flex justify-content-center align-items-center h-100">
        <div class="col">
          <div class="card">
            <div class="card-body p-4">

              <div class="row">

                <div class="col-lg-7">
                  <h5 class="mb-3"><a href="#!" class="text-body"><i
                        class="fas fa-long-arrow-alt-left me-2"></i>Continue shopping</a></h5>
                  <hr>

                  <div class="d-flex justify-content-between align-items-center mb-4">
                    <div>
                      <p class="mb-1">Shopping cart</p>
                      <p class="mb-0">You have {{ $cart->count }} items in your cart</p>
                      <p class="mb-0">Sum: {{ $cart->sum }}</p>
                      @if ($cart->promo)
                      <p class="mb-1">Promo activate: {{$cart->promo->code}}</p>
                      <p class="mb-0">Promo sum: {{ $cart->promo_sum }} </p>
                      @endif
                    </div>
                    <div>
                      <p class="mb-0"><span class="text-muted">Sort by:</span> <a href="#!"
                          class="text-body">price <i class="fas fa-angle-down mt-1"></i></a></p>
                    </div>
                  </div>

                  @foreach ($cart->cartProducts as $item)

                  <div class="card mb-3">
                    <div class="card-body">
                      <div class="d-flex justify-content-between">
                        <div class="d-flex flex-row align-items-center">
                          <div>
                            @if ($item->post->image_path)

                            <img
                            src="{{ $item->post->image_src }}"
                            class="img-fluid rounded-3" alt="Shopping item" style="width: 65px;">
                            @endif
                          </div>
                          <div class="ms-3">
                            <h5>{{ $item->post->title }}</h5>
                            <p class="small mb-0">{{ $item->post->preview }}</p>
                          </div>
                        </div>
                        <div class="d-flex flex-row align-items-center">
                          <div style="width: 100px;">
                            <h5 class="fw-normal mb-0">
                                <form action="{{ route('cart.add') }}" method="POST">
                                    @csrf
                                    <div class="col-xs-3">
                                        <input type="hidden" name="product_id" value="{{ $item->product_id }}" />
                                        <input style="width: 50px;" type="number" name="count" value="{{$item->count}}" onchange="this.form.submit()"/>
                                    </div>
                                </form>

                            </h5>
                          </div>
                          <div style="width: 200px;">
                            <h5 class="mb-0">x1 - {{ $item->price }}$ <br> x{{$item->count}} - {{$item->sum }}$</h5>
                          </div>
                          <div style="width: 80px;">
                            <form action="{{ route('cart.remove', $item ) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <input class="btn btn-danger" type="submit" value="Delete" />
                            </form>

                          </div>
                          <a href="#!" style="color: #cecece;"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach


                </div>
                <div class="col-lg-5">

                  <div class="card bg-primary text-white rounded-3">
                    <div class="card-body">

                      <form method="POST" action="{{ route('cart.promo') }}" class="mt-4">
                        @csrf
                        <div class="form-outline form-white mb-4">
                          <input type="text" id="code" class="form-control form-control-lg" siez="17" name="code"
                            placeholder="Promo" />
                            @error('code')
                                <p class="alert alert-danger">{{$message}}</p>
                            @enderror
                            @if(session()->has('message'))
                                <div class="alert alert-success">
                                    {{ session()->get('message') }}
                                </div>
                            @endif
                          <label class="form-label" for="code">Enter promocode</label>
                        </div>
                        <button type="submit" class="btn btn-info btn-block btn-lg">Check promocode</button>
                      </form>
                      <form action="{{ route('cart.promo.delete', $cart ) }}" method="POST">
                        @method('DELETE')
                        @csrf
                        <input class="btn btn-danger" type="submit" value="Delete" />
                    </form>

                      <hr class="my-4">

                      <button type="button" class="btn btn-info btn-block btn-lg">
                        <div class="d-flex justify-content-between">
                          <span>Total price
                            @if ($sum)
                            <s>{{ $cart->sum }} $</s>
                            {{ $sum }} $
                            @else
                            {{ $cart->sum }} $
                            @endif
                        </span>

                        </div>
                      </button>

                    </div>
                  </div>

                </div>

              </div>

            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
@endsection
