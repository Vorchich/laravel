@extends('admin.layouts.app')
@section('title', 'Posts')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <a class="btn btn-primary" href="{{ route('admin.promo.create') }}" role="button">Create Promocode</a>
         <table class="table">
            <thead>
                <tr>
                      <th scope="col">Code</th>
                       <th scope="col">Discount</th>
                       <th scope="col">Number</th>
                      <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($promos as $promo)
                    <tr>
                        <td>{{$promo->code}}</td>
                        <td>{{$promo->value}} {{$promo->type == PromoConstant::TYPE_PERCENT ? '%' : '$'}}</td>
                        <td>
                            <form action="{{ route('admin.promo.destroy', $promo ) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <input class="btn btn-danger" type="submit" value="Delete" />
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>

        </table>
        {{ $promos->links() }}
    </div>
</main>
@endsection
