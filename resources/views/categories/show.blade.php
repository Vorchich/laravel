@extends('layouts.app')
@section('title', 'Posts')
@section('content')
<main id="main" class="main">
    <div class="pagetitle">
        <h1>Category name: {{$category->name}}</h1>
        <a href=" {{ route('categories.index') }} " class="btn btn-primary" >Go to categories</a>
    </div>
</main>
@endsection
