<div class="card">

    <div class="card-body">
        <h5 class="card-title">{{ $post->title }}</h5>
        <p class="card-text">{{ $post->preview }}</p>
        <img class="mt-1" style="width:400px;height:auto;" src="{{ Storage::url($post->image_path) }}"
            class="card-img-top" alt="{{ $post->title }}">
        <form class="col-xs-3" action="{{ route('cart.add') }}" method="POST">
            @csrf
            <input type="hidden" name="product_id" value="{{ $post->id }}" />
            <input style="width: 50px;" type="hidden" name="count" value="1" />
            <button type="submit" class="m-1 btn btn-success">Add to cart</button>

        </form>
        <a href="{{ route('category.post.show', [$post->category_id, $post]) }}" class="m-1 btn btn-primary">Go to
            item</a>
    </div>
</div>
