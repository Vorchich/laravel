<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

use Illuminate\Database\Eloquent\Casts\Attribute;

class CartProduct extends Model
{
    use HasFactory;

    protected $fillable = [
        'cart_id',
        'product_id',
        'count',
    ];

    public function cart()
    {
        return $this->belongsTo(Cart::class);
    }
    public function post()
    {
        return $this->hasOne(Post::class, 'id', 'product_id');
    }

    protected function price(): Attribute
    {
        return Attribute::make(
            get: fn ($value) =>  $this->post->price,
        );
    }


    protected function sum(): Attribute
    {
        return Attribute::make(
            get: fn ($value) =>  $this->count * $this->price,
        );
    }
}
