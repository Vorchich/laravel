@extends('admin.layouts.app')
@section('title', 'Posts')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <form method="POST" action="{{ route('admin.category.post.store', $category ) }}" enctype="multipart/form-data" >
            @csrf
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" class="form-control" placeholder="Enter title" name="title" value="{{ old('title') }}">
                @error('title')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="preview">Preview:</label>
                <input type="text" class="form-control" placeholder="Enter preview" name="preview" value="{{ old('preview') }}">
                @error('preview')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="price">Price:</label>
                <input type="text" class="form-control" placeholder="Enter price" name="price" value="{{ old('price') }}">
                @error('price')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" id="description" name="description" rows="3">{{ old('description') }}</textarea>
                @error('description')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror
            </div>
            <label for="image">Image:</label>
                <input type="file" class="form-control" name="image">
                @error('image')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror

            <button type="submit" class="btn btn-primary m-3">Submit</button>
        </form>
    </div>
</main>
@endsection
