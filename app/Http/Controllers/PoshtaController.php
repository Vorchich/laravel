<?php

namespace App\Http\Controllers;

use App\Http\Requests\Admin\CityRequest;
use App\Models\CityNovaPoshta;
use App\Models\Warehouse;
use Illuminate\Http\Request;


class PoshtaController extends Controller
{
    public function getCity(CityRequest $req)
    {
        //dd($city->validated());
        return json_encode(CityNovaPoshta::where('name','like',$req->city.'%')->get(['name','city_ref']),JSON_UNESCAPED_UNICODE);
    }

    public function getWarehouse(Request $req)
    {
        $city = $req->city;
        $name = $req->name;
        $number = $req->number;

        return json_encode(
        Warehouse::query()
            ->when($city, function ($builder) use($city){
            $builder->where('city_id', 'like', $city);
            })
            ->when($name, function ($builder) use($name){
            $builder->where('name', 'like', $name.'%');
            })
            ->when($number, function($builder) use($number){
                $builder->where('number', '=', $number);
            })
            ->get(['name','ref','number'])
        ,JSON_UNESCAPED_UNICODE);


        //     return OrderProducts::query()
        // ->when($start, function ($builder) use($start) {
        //     $builder->where('created_at', '>', $start);
        // })
        // ->when($end, function ($builder) use($end) {
        //     $builder->where('created_at', '<', $end);
        // })
        // ->get()
        // ->countBy('name')
        // ->take($top);
        // );
    }
}
