@extends('layouts.app')
@section('title', 'Posts')
@section('content')
<main id="main" class="main">
    <div class="pagetitle">
        @foreach ($categories as $category)
            @include('parts.category', compact('category'))
        @endforeach
        {{ $categories->links() }}
    </div>
</main>
@endsection
