<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreOrderRequest;
use App\Models\Cart;
use App\Models\Order;
use App\Models\Category;
use App\Models\OrderProducts;
use App\Services\CartService;

class OrderController extends Controller
{
    public function create(StoreOrderRequest $req, CartService $service)
    {
        $categories = Category::get();
        $cart = $service->cart;
        $order = Order::create([
            'sum' => $cart->sum,
            'name' => $req->name ?? auth()->user()->name,
            'user_id' => auth()->id(),
            'promo_id' => $cart->promo->id ?? null,
        ]);

        if (! auth()->check()) {
            session(['order_id' => $order->id]);
        }

        foreach($cart->cartProducts as $product){
            OrderProducts::create([
                'order_id' => $order->id,
                'product_id' => $product->product_id,
                'count' => $product->count,
                'price' => $product->price,
                'name' => $product->post->title,
            ]);
        }

        $cart->delete();

        return redirect()->route('order.show', compact('categories'));
    }

    public function show()
    {
        $categories = Category::get();
        if(auth()->check())
        {
            $orders = auth()->user()->orders;
        } else
        {
            $orders = array(Order::findOrFail(session('order_id')));
        }

        return view('part.orders', compact('orders', 'categories'));
    }
}
