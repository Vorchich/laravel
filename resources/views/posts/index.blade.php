@extends('layouts.app')
@section('title', 'Posts')
@section('content')
@include('parts.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        @foreach ($posts as $post)
            @include('parts.item', compact('post'))
        @endforeach
        {{ $posts->links() }}
    </div>
</main>
@endsection
