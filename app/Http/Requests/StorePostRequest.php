<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StorePostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }


    protected function prepareForValidation()
    {
        $this->merge([
            'user_id' => $this->user()->id,
            'likes' => 0,
            'posted' => 0,
        ]);
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'title'         => ['required','string','min:5','max:30',],
            'preview'       => ['required','string','min:5','max:255'],
            'description'   => ['required','string','min:5'],
            'price'         => ['sometimes', 'integer'],
            'image'         => ['mimes:jpg,png,jpeg','max:5048'],
            'user_id'       => ['required', 'integer',],
            'likes'         => ['required', 'integer'],
            'posted'        => ['required', 'integer'],
        ];
    }
}
