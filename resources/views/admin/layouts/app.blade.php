<!DOCTYPE html>
<html lang="en">

<head>
    @include('admin.part.head')

    @livewireStyles

</head>

<body>

    @include('admin.part.header')

    @yield('content')

    @include('admin.part.footer')

    @include('admin.part.bodyscripts')

    @livewireStyles

</body>
</html>
