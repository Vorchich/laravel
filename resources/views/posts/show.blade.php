@extends('layouts.app')
@section('title', 'Posts')
@section('content')
@include('parts.sidebar')
<main id="main" class="main">
    <div class="pagetitle">

<div class="card" style="width: 60rem">
    @if ($post->image_path)
    <div >
        <img style="width:400px;height:auto;" src="{{ $post->image_src }}" class="m-1 card-img-top" alt="img">
    </div>
    @endif

    <div class="card-body">

        <h5 class="card-title">{{ $post->title }}</h5>
        <p class="card-text">{{ $post->preview }}</p>
        <p class="card-text">Price: {{ $post->price }}$</p>

        {{-- <p class="card-text">author: {{ $post->user ? $post->user->name : 'без автора' }}</p> --}}
        {{-- @if ((auth()->user())) --}}
        <form action="{{ route('cart.add') }}" method="POST">
            @csrf
            <input type="hidden" name="product_id" value="{{ $post->id }}" />
            <div>Enter count</div>
            <input type="text" name='count' class="m-1 form-control" value="1" />

            <button type="submit" class="m-1 btn btn-primary">Add to cart</button>
        </form>
        {{-- @endif --}}
    </div>
    <a href="{{ route('category.index') }}" class="btn btn-primary">Go to categories</a>
  </div>

    </div>
</main>
@endsection
