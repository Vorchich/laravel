<aside id="sidebar" class="sidebar">

    <ul class="sidebar-nav" id="sidebar-nav">
        @foreach ($categories as $cat)
        <li class="nav-item">
            <a class="nav-link collapsed" href="{{ route('category.post.index', $cat) }}">
                <i class="bi bi-dash-circle"></i>
                <span>{{$cat->name}}</span>
            </a>
        </li>
        @endforeach
        <!-- End Error 404 Page Nav -->

    </ul>

</aside><!-- End Sidebar-->
