@extends('admin.layouts.app')
@section('title', 'Create category')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <form method="POST" action=" {{route('admin.promo.store') }}">

            @csrf
            <div class="form-check">
                <input class="form-check-input"  {{ old('check') == 'on' ? 'checked' : '' }}
                type="checkbox" id="check" name='check'>
                <label class="form-check-label" for="flexCheckIndeterminate">
                  Random promocode generation
                </label>
              </div>
            <div class="row mb-3">
                <label for="code" class="col-sm-2 col-form-label">Promocode:</label>
                    <div class="col-sm-3">
                        <input value="{{ old('code') }}" type="string" class="form-control" id="code" placeholder="Enter promocode" name="code">
                            @error('code')
                                <p class="alert alert-danger">{{$message}}</p>
                            @enderror
                    </div>
            </div>
            <div class="row mb-3">
                <label for="interest" class="col-sm-2 col-form-label">Type:</label>
                    <div class="col-sm-3">
                        <select name="type" id="interest">
                            @foreach ($types as $type)
                            <option value="{{$type}}">@lang('promo.types.'.$type)</option>
                            @endforeach
                        </select>
                            @error('type')
                                <p class="alert alert-danger">{{$message}}</p>
                            @enderror
                    </div>
            </div>
            <div class="row mb-3">
                <label for="num" class="col-sm-2 col-form-label">Value:</label>
                    <div class="col-sm-3">
                        <input type="string" value ="{{  old('value')  }}" class="form-control" id="num" placeholder="Enter Sum" name="value">
                            @error('value')
                                <p class="alert alert-danger">{{$message}}</p>
                            @enderror
                    </div>
            </div>
            <div class="row mb-3">
                <label for="number" class="col-sm-2 col-form-label">Number:</label>
                    <div class="col-sm-3">
                        <input type="string" class="form-control" value ="{{  old('number')  }}" id="number" placeholder="Enter number" name="number">
                            @error('number')
                                <p class="alert alert-danger">{{$message}}</p>
                            @enderror
                    </div>
            </div>

                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Create promocode</button>
                </div>
        </form>
    </div>
</main>
@endsection
