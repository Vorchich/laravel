@extends('admin.layouts.app')
@section('title', 'Posts')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
<div class="card" style="width: 40rem;">

    @if ($post->image_path)
    <div >
        <img src="{{ Storage::url($post->image_path) }}" class="card-img-top" alt="{{ $post->title }}">
    </div>
    @endif

    <div class="card-body">

        <h5 class="card-title">{{ $post->title }}</h5>
        <p class="card-text">{{ $post->description }}</p>
        <p class="card-text">author: {{ $post->user ? $post->user->name : 'без автора' }}</p>
        @if (Auth::check() && Auth::user()->admin)
        <div class="d-flex justify-content-between">
            <a href="{{ route('admin.category.post.index', $post->category) }}" class="btn btn-primary">Go to items</a>
            <a href="{{ route('admin.category.post.edit', [$post->category, $post]) }}" class="btn btn-primary">Update</a>
            <form action="{{ route('admin.category.post.destroy', [$post->category, $post]) }}" method="POST">
                @method('DELETE')
                @csrf
                <input class="btn btn-danger" type="submit" value="Delete" />
            </form>
        </div>
      @endif


    </div>
  </div>

    </div>
</main>
@endsection
