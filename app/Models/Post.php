<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Storage;

use Illuminate\Database\Eloquent\Casts\Attribute;

class Post extends Model
{
    use HasFactory;

    protected $fillable = [
        'title',
        'preview',
        'description',
        'price',
        'image_path',
        'user_id',
        'category_id',
        'likes',
        'posted',
    ];

    public function category()
    {
        return $this->belongsTo(Category::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function cartProduct()
    {
        return $this->belongsTo(CartProduct::class, 'product_id', 'id');
    }

    public function orderProduct()
    {
        return $this->belongsTo(OrderProduct::class, 'product_id', 'id');
    }

    protected function imageSrc(): Attribute
    {
        return Attribute::make(
            get: fn ($value) =>  Storage::disk('public')->url($this->image_path),
        );
    }

    // protected function getImageSrcAttribute()
    // {
    //     return Storage::disk('public')->url($this->image_path);
    // }

}
