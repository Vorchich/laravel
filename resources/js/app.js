
import './bootstrap';

require('../../public/vendor/apexcharts/apexcharts.min.js')
require('../../public/vendor/chart.js/chart.js')
require('../../public/vendor/echarts/echarts.min.js')
require('../../public/vendor/bootstrap/js/bootstrap.bundle')
require('../../public/vendor/quill/quill.min.js')
require('../../public/vendor/simple-datatables/simple-datatables')
require('../../public/vendor/tinymce/tinymce.min.js')
require('../../public/vendor/php-email-form/validate')
require('./main')

