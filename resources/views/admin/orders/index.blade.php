@extends('admin.layouts.app')
@section('title', 'Posts')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
         <table class="table">
            <thead>
                <tr>
                  <th scope="col">Id</th>
                      <th scope="col">Sum</th>
                       <th scope="col">Name</th>
                      <th scope="col">Status</th>
                      <th scope="col">Update status</th>
                      <th scope="col">Created at</th>
                </tr>
            </thead>
            <tbody>

                @foreach ($orders as $order)
                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->sum}} $</td>
                        <td>{{$order->name}}</td>
                        <td>{{$order->status}}</td>
                        <td><a href="{{ route('admin.order.edit', $order) }}" class="btn btn-success">Update</a></td>
                        <td>{{$order->created_at}}</td>
                    </tr>
                @endforeach
            </tbody>

        </table>
        {{ $orders->links() }}
    </div>
</main>
@endsection
