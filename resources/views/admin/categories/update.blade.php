@extends('admin.layouts.app')
@section('title', 'Post update')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <form method="POST" action=" {{route('admin.category.update', $category) }}">
            @method('PUT')
            @csrf
            <div class="row mb-3">
                <label for="name" class="col-sm-2 col-form-label">Category:</label>
                    <div class="col-sm-10">
                        <input type="string" class="form-control" value="{{ $category->name }}" id="category" placeholder="Enter name category" name="name" >
                            @error('name')
                                <p class="alert alert-danger">{{$message}}</p>
                            @enderror
                    </div>
                </div>
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Submit Form</button>
                </div>
        </form>
    </div>
</main>
@endsection
