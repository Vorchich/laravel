<?php

namespace App\Providers;

use App\Services\StatisticService;
use Illuminate\Support\ServiceProvider;

class StatisticProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->singleton(StatisticService::class, function ($app) {
            return new StatisticService();
        });
    }
}
