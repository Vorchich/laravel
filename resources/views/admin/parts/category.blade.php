
<div class="card">

    <div class="card-body">
      <h5 class="card-title">{{ $category->name }}</h5>
      <p class="card-text">posts count: {{ $category->posts_count }}</p>
      <a href="{{ route('admin.category.show', $category) }}" class="btn btn-primary">Go to category</a>

      @foreach ($category->posts as $post)
          @include('admin.parts.item', compact('post'))
      @endforeach
    </div>
  </div>
