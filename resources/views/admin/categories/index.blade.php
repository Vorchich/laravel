@extends('admin.layouts.app')
@section('title', 'Posts')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <a class="btn btn-primary" href="{{route('admin.category.create') }}" role="button">Create Category</a>
         <table class="table">
            <thead>
                <tr>
                  <th scope="col">Id</th>
                      <th scope="col">Category</th>
                       <th scope="col">Item Count</th>
                      <th scope="col">Created at</th>
                      <th scope="col"></th>
                      <th scope="col"></th>
                </tr>
            </thead>
            <tbody>
                @foreach ($categories as $category)
                    <tr>
                        <td>{{$category->id}}</td>
                        <td>{{$category->name}}</td>
                        <td>{{$category->posts_count}}</td>
                        <td>{{$category->created_at}}</td>
                        <td>
                                <a class="btn btn-primary " href="{{route('admin.category.post.index',$category)}}">
                                    <span>Go to category</span>
                                </a>
                        </td>
                        <td>
                            <form action="{{ route('admin.category.destroy', $category) }}" method="post">
                                @method('DELETE')
                                @csrf
                                <button type="submit" class="btn btn-danger">Delete</button>
                            </form>
                        </td>
                    </tr>
                @endforeach
            </tbody>

        </table>
        {{ $categories->links() }}
    </div>
</main>
@endsection
