<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Casts\Attribute;

class Promo extends Model
{
    use HasFactory;

    const TYPE_PERCENT = 'percent';
    const TYPE_NOMINAL = 'nominal';

    protected $fillable = [
        'code',
        'value',
        'type',
        'number',

    ];

    static public function getTypes()
    {
        return [
            self::TYPE_NOMINAL,
            self::TYPE_PERCENT,
        ];
    }

    protected function count(): Attribute
    {
        return Attribute::make(
            get: fn ($value) =>  $this->number--,
        );
    }
}
