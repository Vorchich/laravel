<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    use HasFactory;

    const STATUS_NEW = 'New';
    const STATUS_REFUSE = 'Refuse';
    const STATUS_DONE = 'Done';

    protected $fillable =[
        'sum',
        'name',
        'status',
        'user_id',
        'promo',
    ];

    static public function getTypes()
    {
        return [
            self::STATUS_NEW,
            self::STATUS_REFUSE,
            self::STATUS_DONE,
        ];
    }

    public function users()
    {
        return $this->belongsTo(User::class,'id', 'user_id');
    }
    public function orderProducts()
    {
        return $this->hasMany(OrderProducts::class, 'order_id', 'id');
    }
}
