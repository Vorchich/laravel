
<div class="card">

    <div class="card-body">
        <img class="mt-1" style="width:400px;height:auto;" src="{{ Storage::url($post->image_path) }}" class="card-img-top" alt="{{ $post->title }}">
        <h5 class="card-title">{{ $post->title }}</h5>
        <p class="card-text">{{ $post->preview }}</p>
        <p class="card-text">Price: {{ $post->price }}$</p>
        <a href="{{ route('admin.category.post.show', [$category, $post]) }}" class="btn btn-primary">Go to item</a>

    </div>
  </div>
