<?php

namespace App\Services;

use App\Models\Cart;
use App\Models\CartProduct;
use App\Models\Promo;
use Illuminate\Support\Str;

class CartService
{
    public $cart;
    public $sum;
    
    public function __construct()
    {
                if (! session('identify')) {
                    $ident = Str::uuid()->toString();
                    session(['identify' => $ident]);
                }
                    $ident = session('identify');
                $this->cart = Cart::firstOrCreate([
                    'identify' => $ident,
                ]);
                if(auth()->check())
                {
                    $ident = session('identify');
                    $this->cart = Cart::updateOrCreate([
                        'user_id' => auth()->id(),
                    ],
                        ['identify' => $ident,
                    ]);
                        $cart = Cart::where(['identify' => $ident, 'user_id' => null])->first();
                        if(($cart)){
                            foreach($cart->cartProducts as $product)
                            {
                                $this->cart->cartProducts()->updateOrCreate([
                                    'product_id' => $product->product_id,
                                ], [
                                    'count' => $product->count,
                                ]);
                            }
                            $cart->delete();
                        }
                }
    }

    public function updateOrCreate($product_id, $count = 1)
    {
            $product = $this->cart->cartProducts()->updateOrCreate([
                'product_id' => $product_id,
            ], [
                'count' => $count,
            ]);

            return $product;
    }

    public function delete($id)
    {
        $this->cart->cartProducts()->findOrFail($id)->delete();
    }

    public function promoAddOrCreate($promo)
    {
        if($this->cart->cartProducts->count()){
            $this->cart->update(['promo_id'=> $promo->id]);
        }

        return $this->cart;
    }

    public function promoSum($sum)
    {
        if ($this->cart->promo_id) {
            $this->sum = $sum;
            switch ($this->cart->promo->type) {

                case Promo::TYPE_NOMINAL:
                    $this->sum -= $sum;
                    break;
                case Promo::TYPE_PERCENT:

                    $this->sum *= (100 - $this->cart->promo->value) / 100;

                    break;
            }
            $sum = $sum < 0 ? 0 : $sum;

            return $this->sum;
        }
    }

    public function promoDelete()
    {
        $this->cart->update(['promo_id' => null]);

        return $this->cart;
    }
}
