@extends('admin.layouts.app')
@section('title', 'Create category')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <table class="table">
            <thead>
                <tr>
                  <th scope="col">Id</th>
                      <th scope="col">Sum</th>
                       <th scope="col">Name</th>
                      <th scope="col">Status</th>
                      <th scope="col">Update status</th>
                      <th scope="col">Created at</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                    <td>{{$order->id}}</td>
                    <td>{{$order->sum}}</td>
                    <td>{{$order->name}}</td>
                    <td>{{$order->status}}</td>
                    <td>
                        <form action="{{ route('admin.order.update', $order) }}" method="POST">
                        @csrf
                        @method('PUT')
                        <select name="status" id="status">
                            @foreach ($statuses as $status)
                            <option @if ($order->status == $status)
                                selected="selected"
                            @endif value="{{ $status }}">{{$status}}</option>
                            @endforeach
                        </select>
                        <input class="btn btn-danger" type="submit" value="Update status" />
                    </form>
                    </td>
                    <td>{{$order->created_at}}</td>
                </tr>
            </tbody>

        {{-- <form method="POST" action=" {{route('admin.order.update', $order) }}">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="title">Id:</label>
                <input type="text" class="form-control" placeholder="Enter title" name="title" value="{{ $order->id }}">
            </div>
            <div class="form-group">
                <label for="preview">Status:</label>
                <input type="text" class="form-control" placeholder="Enter preview" name="preview" value="{{ $order->Status }}">
                @error('preview')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror
            </div>

            <button type="submit" class="btn btn-primary m-3">Submit</button>
        </form> --}}
    </div>
</main>
@endsection
