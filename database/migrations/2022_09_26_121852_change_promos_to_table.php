<?php

use App\Models\Promo;
use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Promo::truncate();
        Schema::table('promos', function (Blueprint $table) {
            $table->dropColumn('interest');
            $table->dropColumn('num');
            $table->string('type');
            $table->decimal('value',6,2);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Promo::truncate();
        Schema::table('promos', function (Blueprint $table) {
            $table->decimal('interest');
            $table->decimal('num');
            $table->dropColumn('type');
            $table->dropColumn('value');
        });
    }
};
