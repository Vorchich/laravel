@extends('admin.layouts.app')
@section('title', 'Posts')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <form action="{{ route('admin.statistic') }}" method="GET">
                <label for="start">Start date:</label>
            <input type="date" id="start" name="start"
                value="{{ request()->start }}"
                min="2020-01-01" max="2040-12-31">
                <label for="end">End date:</label>
            <input type="date" id="end" name="end"
                value="{{ request()->end }}"
                min="2020-01-01" max="2040-12-31">
                    <label for="interest" class="col-sm-2 col-form-label">Top:</label>
                    <select name="top" id="top">
                        <option  value="1">1</option>
                        <option
                         @if( request()->top == 5)
                         selected="selected"
                         @endif
                            value="5">5</option>
                        <option
                        @if( request()->top == 10)
                         selected="selected"
                         @endif value="10">10</option>
                    </select>
            <input class="btn btn-primary" type="submit" value="Set date" />
        </form>
        <table class="table">
            <thead>
                <tr>
                  <th scope="col">Done</th>
                      <th scope="col">Refused</th>
                       <th scope="col">New</th>
                </tr>
            </thead>
            <tbody>
                    <tr>
                        <td>{{ $order_statistic['Done'] ?? 0}} </td>
                        <td>{{ $order_statistic['Refuse'] ?? 0}} </td>
                        <td>{{ $order_statistic['New'] ?? 0}} </td>
                    </tr>
            </tbody>

        </table>
        <table class="table">
            <thead>
                <tr>
                  <th scope="col">Name</th>
                      <th scope="col">Count</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($products_statistic as $name => $value)
                    <tr>
                        <td> {{$name}}</td>
                        <td> {{$value}}</td>
                        @endforeach
                    </tr>
            </tbody>

        </table>

    </div>
</main>
@endsection
