<?php

namespace App\Http\Controllers;
use Phpfastcache\Helper\Psr16Adapter;

use App\Models\Category;

class CategoryController extends Controller
{
    public function index()
    {

        // $instagram = InstagramScrapper::withCredentials(env('INSTA_USER'), env('INSTA_PASS'), new PSRCacheInterface());
        // $media = $instagram->getAccount('gigaprog');
        //$nonPrivateAccountMedias = $instagram->getMediasByUrl('https://www.instagram.com/p/CejJqPXqpnP/');
        //$media = $instagram->getMediaById('2856169073822833103');
        // dd($media);
        // $categories = Category::withCount('posts')
        //                     ->with('posts.user')
        //                     ->latest()
        //                     ->simplePaginate(3);
        // return view('categories.index',compact('categories'));
    }

    public function show(Category $category)
    {
        return view('categories.show', compact('category'));
    }
}
