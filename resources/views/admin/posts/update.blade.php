@extends('admin.layouts.app')
@section('title', 'Create category')
@section('content')
@include('admin.components.sidebar')
<main id="main" class="main">
    <div class="pagetitle">
        <form method="POST" action=" {{route('admin.category.post.update', [$post->category, $post]) }}"
            enctype="multipart/form-data">
            @method('PUT')
            @csrf
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" class="form-control" placeholder="Enter title" name="title" value="{{ $post->title }}">
                @error('title')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="preview">Preview:</label>
                <input type="text" class="form-control" placeholder="Enter preview" name="preview" value="{{ $post->preview }}">
                @error('preview')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <textarea class="form-control" id="description" name="description" rows="3">{{ $post->description }}</textarea>
                @error('description')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror
            </div>
            <label for="image">Image:</label>
            <div class="m-3" style="width: 40rem;" >
                <img src="{{ Storage::url($post->image_path) }}" class="card-img-top" alt="img">
            </div>
                <input type="file" class="form-control" name="image">

                @error('image')
                    <p class="alert alert-danger">{{$message}}</p>
                @enderror

            <button type="submit" class="btn btn-primary m-3">Submit</button>
        </form>
    </div>
</main>
@endsection
