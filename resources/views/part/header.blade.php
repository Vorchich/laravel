<header id="header" class="header fixed-top d-flex align-items-center">

    <div class="d-flex align-items-center justify-content-between">
        <a href="{{route('home')}}" class="logo d-flex align-items-center">
            <span class="d-none d-lg-block">Ecommerce</span>
        </a>

    </div><!-- End Logo -->

    {{-- <div class="search-bar">
        <form class="search-form d-flex align-items-center" method="POST" action="#">
            <input type="text" name="query" placeholder="Search" title="Enter search keyword">
            <button type="submit" title="Search"><i class="bi bi-search"></i></button>
        </form>
    </div><!-- End Search Bar --> --}}
    <nav class="header-nav ms-auto">
        <ul class="d-flex align-items-center">

            <li class="nav-item d-block d-lg-none">
                <a class="nav-link nav-icon search-bar-toggle " href="/">
                    <i class="bi bi-search"></i>
                </a>
            </li><!-- End Search Icon-->
            @if ((session('order_id')&&!(Auth::check())))
            <li class="nav-item dropdown">
                <a class="nav-link nav-icon search-bar-toggle " href="{{ route('order.show') }}">
                    <i class="bi bi-bag"></i>
                </a>
            </li>
            @endif

            <li class="nav-item dropdown">

                <a class="nav-link nav-icon" href="/" data-bs-toggle="dropdown">
                    <i class="bi bi-cart"></i>
                    <span class="badge bg-primary badge-number">{{ $cart->count }}</span>
                </a><!-- End Notification Icon -->

                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow notifications">

                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li class="notification-item">
                        <div>
                            @foreach ($cart->cartProducts as $item)

                  <div class="card mb-3">
                    <div class="card-body">
                      <div class="d-flex justify-content-between">
                        <div class="d-flex flex-row align-items-center">
                          <div>
                            @if ($item->post->image_path)

                            <img
                            src="{{ $item->post->image_src }}"
                            class="img-fluid rounded-3" alt="Shopping item" style="width: 65px;">
                            @endif
                          </div>
                          <div class="ms-3">
                            <h5>{{ $item->post->title }}</h5>
                            <p class="small mb-0">{{ $item->post->preview }}</p>
                          </div>
                        </div>
                        <div class="d-flex flex-row align-items-center">
                          <div style="width: 100px;">
                            <h5 class="fw-normal mb-0">
                                <form action="{{ route('cart.add') }}" method="POST">
                                    @csrf
                                    <div class="col-xs-3">
                                        <input type="hidden" name="product_id" value="{{ $item->product_id }}" />
                                        <input style="width: 50px;" type="number" name="count" value="{{$item->count}}" onchange="this.form.submit()"/>
                                    </div>
                                </form>

                            </h5>
                          </div>
                          <div style="width: 200px;">
                            <h5 class="mb-0">x1 - {{ $item->price }}$ <br> x{{$item->count}} - {{$item->sum }}$</h5>
                          </div>
                          <div style="width: 80px;">
                            <form action="{{ route('cart.remove', $item ) }}" method="POST">
                                @method('DELETE')
                                @csrf
                                <input class="btn btn-danger" type="submit" value="Delete" />
                            </form>
                          </div>
                          <a href="#!" style="color: #cecece;"><i class="fas fa-trash-alt"></i></a>
                        </div>
                      </div>
                    </div>
                  </div>
                  @endforeach
                        </div>
                    </li>
                    <li class="dropdown-header">
                        You have {{ $cart->count }} products in cart
                        <form method="POST" action="{{ route('order.create') }}">
                            @csrf
                            <input type="hidden" name="sum" value="{{ $cart->sum }}">
                            <input type="hidden" name="id" value="{{ $cart->id }}">
                            @if (!(auth()->check()))
                            <input placeholder="name" type="text" name="name" >
                            @endif
                            <button
                            @if($cart->sum == 0) @disabled(true)@endif

                                class="btn btn-primary btn-block btn-lg">
                                {{ $cart->sum }}$ Checkout <i class="fas fa-long-arrow-alt-right"></i>
                              </button>

                        </form>

                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                </ul><!-- End Notification Dropdown Items -->

            </li><!-- End Notification Nav -->
            @if(Auth::check())


            <li class="nav-item dropdown">

                <a class="nav-link nav-icon" href="#" data-bs-toggle="dropdown">
                    <i class="bi bi-chat-left-text"></i>
                    <span class="badge bg-success badge-number">3</span>
                </a><!-- End Messages Icon -->

                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow messages">
                    <li class="dropdown-header">
                        You have 3 new messages
                        <a href="#"><span class="badge rounded-pill bg-primary p-2 ms-2">View all</span></a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li class="message-item">
                        <a href="#">
                            <img src="{{ asset('/images/messages-1.jpg') }}" alt="" class="rounded-circle">
                            <div>
                                <h4>Maria Hudson</h4>
                                <p>Velit asperiores et ducimus soluta repudiandae labore officia est ut...</p>
                                <p>4 hrs. ago</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li class="message-item">
                        <a href="#">
                            <img src="{{ asset('/images/messages-2.jpg') }}" alt="" class="rounded-circle">
                            <div>
                                <h4>Anna Nelson</h4>
                                <p>Velit asperiores et ducimus soluta repudiandae labore officia est ut...</p>
                                <p>6 hrs. ago</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li class="message-item">
                        <a href="#">
                            <img src="{{ asset('/images/messages-3.jpg') }} alt="" class="rounded-circle">
                            <div>
                                <h4>David Muldon</h4>
                                <p>Velit asperiores et ducimus soluta repudiandae labore officia est ut...</p>
                                <p>8 hrs. ago</p>
                            </div>
                        </a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li class="dropdown-footer">
                        <a href="#">Show all messages</a>
                    </li>

                </ul><!-- End Messages Dropdown Items -->

            </li><!-- End Messages Nav -->

            <li class="nav-item dropdown pe-3">

                <a class="nav-link nav-profile d-flex align-items-center pe-0" href="/profile"
                    data-bs-toggle="dropdown">
                    <img src="{{ asset('/images/profile-img.jpg') }}" alt="Profile" class="rounded-circle">
                    <span class="d-none d-md-block dropdown-toggle ps-2">{{Auth::user()->name}}</span>
                </a><!-- End Profile Iamge Icon -->

                <ul class="dropdown-menu dropdown-menu-end dropdown-menu-arrow profile">
                    <li class="dropdown-header">
                        <h6>{{Auth::user()->name}}</h6>
                        <span>Web Designer</span>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li>
                        <a class="dropdown-item d-flex align-items-center" href="/profile">
                            <i class="bi bi-person"></i>
                            <span>My Profile</span>
                        </a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li>
                        <a class="dropdown-item d-flex align-items-center" href="/profile">
                            <i class="bi bi-gear"></i>
                            <span>Account Settings</span>
                        </a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li>
                        <a class="dropdown-item d-flex align-items-center" href="{{ route('order.show') }}">
                            <i class="bi bi-question-circle"></i>
                            <span>Orders</span>
                        </a>
                    </li>
                    <li>
                        <hr class="dropdown-divider">
                    </li>

                    <li>
                        <form id="logout-form" action="{{ route('logout') }}" method="POST">
                            {{ csrf_field() }}
                            <button type="submit" class="dropdown-item d-flex align-items-center" ><i class="bi bi-box-arrow-right"></i>Logout</button>
                        </form>

                    </li>

                </ul><!-- End Profile Dropdown Items -->
            </li><!-- End Profile Nav -->
            @else
                @if(!Route::is('login'))
                    <a class="dropdown-item  d-flex align-items-center" href="{{ route('login')}}">
                        <span>Login</span>
                    </a>
                @endif
                @if(!Route::is('register'))
                    <a class="dropdown-item d-flex align-items-center" href="{{ route('register')}}">
                        <span>Register</span>
                    </a>
                @endif
        @endif
        </ul>

    </nav><!-- End Icons Navigation -->

</header><!-- End Header -->
