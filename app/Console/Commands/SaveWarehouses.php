<?php

namespace App\Console\Commands;

use App\Models\CityNovaPoshta;
use App\Models\Warehouse;
use Illuminate\Console\Command;
use Illuminate\Support\Facades\Http;

class SaveWarehouses extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'warehouse:update';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Update city_nova_poshtas table';

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $cities = CityNovaPoshta::all();
        foreach($cities as $city){
            $page = 1;
            do {
                $data = [
                        'modelName' => "Address",
                        'calledMethod' => "getWarehouses",
                        'methodProperties' => [
                            'CityRef' => $city->city_ref,
                            'Limit' => 450,
                            'Page' => $page,
                            'Language' => 'UA'
                        ]
                    ];
                $warehouses = Http::post('http://api.novaposhta.ua/v2.0/json/', $data);
                $warehouses = $warehouses->json();
                foreach($warehouses['data'] as $warehouse){
                    Warehouse::updateOrCreate([
                        'ref' => $warehouse['Ref'],
                    ],[
                        'name' => $warehouse['Description'],
                        'number' => $warehouse['Number'],
                        'city_id' => $city['id'],
                    ]);
            }
            $page++;
            }while(!empty($warehouses['data']));
        }
    }
}
