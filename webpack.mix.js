const mix = require('laravel-mix');

/*
 |--------------------------------------------------------------------------
 | Mix Asset Management
 |--------------------------------------------------------------------------
 |
 | Mix provides a clean, fluent API for defining some Webpack build steps
 | for your Laravel applications. By default, we are compiling the CSS
 | file for the application as well as bundling up all the JS files.
 |
 */

mix.js('resources/frontend/js/app.js', 'public/frontend/admin/js')
// .combine('resources/frontend/vendor/**/*.js', 'public/frontend/admin/js/app.js')
.sass('resources/frontend/sass/app.scss', 'public/frontend/admin/css')
.version()
