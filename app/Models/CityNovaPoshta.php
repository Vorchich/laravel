<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CityNovaPoshta extends Model
{
    use HasFactory;

    protected $fillable =[
        'name',
        'city_ref',
    ];

    public function warehouses()
    {
        return $this->hasMany(Warehouse::class);
    }
}
