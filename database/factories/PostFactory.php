<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Post;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;
use Illuminate\Http\File;
/**
 * @extends \Illuminate\Database\Eloquent\Factories\Factory<\App\Models\Post>
 */
class PostFactory extends Factory
{
    /**
     * Define the model's default state.
     *
     * @return array<string, mixed>
     */
    public function definition()
    {
        return [
            'title' => $this->faker->title(),
            'preview' => $this->faker->sentence(),
            'description' => $this->faker->text(),
            'preview' => $this->faker->sentence(),
            'image_path' => $this->faker->imageUrl(500,500),
            'created_at' => now(),
            'user_id' => $this->faker->numberBetween(1,2),
            'category_id' => $this->faker->numberBetween(900,1000),
            'likes' => 0,
            'posted' => 0,
        ];
    }
}
