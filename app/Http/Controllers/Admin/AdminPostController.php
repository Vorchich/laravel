<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Http\Requests\StorepostRequest;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class AdminpostController extends Controller
{
    public function index(Category $category)
    {
        $posts = $category->posts()->latest('created_at')->paginate();

        return view('admin.posts.index', compact('posts', 'category'));
    }

    public function create(Category $category)
    {
        return view('admin.posts.create', compact('category'));
    }

    public function store(StorepostRequest $request, Category $category)
    {
        $data = $request->validated();
        if ($request->has('image')) {
            $data['image_path'] = $request->image->store('images', 'public');
        }

        $category->posts()->create($data);

        return redirect()->route('admin.category.post.index', $category);
    }

    public function show(Category $category, Post $post)
    {
        return view('admin.posts.show', compact('post'));
    }

    public function edit(Category $category, Post $post)
    {
        return view('admin.posts.update', compact('post'));
    }

    public function update(StorepostRequest $request, Category $category, Post $post)
    {

        $val = array_merge($request->validated(), [
            // 'image_path'=> $request->image->store('images', 'public'),
            'user_id'=>auth()->id(),
            'likes' => 0,
            'posted' =>0,
        ]);

        if ($request->has('image')) {
            Storage::disk('public')->delete($post->image_path);
            $val['image_path'] = $request->image->store('images', 'public');
        }
        $post->update($val);

        return redirect()->route('admin.category.post.index', ['category' => $category]);
    }

    public function destroy(Category $category, Post $post)
    {
        if($post->image_path){

            Storage::disk('public')->delete($post->image_path);
        }
        $post->delete();

        return redirect()->route('admin.category.post.index', $category);
    }
}
