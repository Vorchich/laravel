<?php

namespace App\Http\Requests;

use App\Models\Promo;
use Illuminate\Foundation\Http\FormRequest;

class StorePromoRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array<string, mixed>
     */
    public function rules()
    {
        return [
            'check'     => ['sometimes'],
            'code'      => ['nullable','min:3'],
            'type'  => ['required', 'in:'.implode(',', Promo::getTypes())],
            'value'       => ['required','numeric','min:1'],
            'number'    => ['required','numeric','min:1'],
        ];
    }
}
